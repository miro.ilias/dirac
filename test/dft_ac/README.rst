DFT polarizability test
=======================

This test calculates the DFT static electric dipole polarizability of Ne atom
using the GRAC and SAOP asymptotic correction schemes.

The chosen functionals are GRAC: PBE0gracLB94
                        and SAOP: GLLBsaopLBalpha

We assume that the basis sets are available in your standard path.
These sets can be obtained from the Dalton distribution and may later be included in the DIRAC distribution as well.

Note that a coarse DFT grid and a poor convergence threshold are used.


